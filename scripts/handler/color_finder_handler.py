lstcolors=[]
class ColorFinder:
    def unique_colors(self, data):
        if not isinstance(data, str):
            for k, v in data.items():
                if isinstance(v, dict):
                    self.unique_colors(v)
                elif hasattr(v, '__iter__') and not isinstance(v, str):
                    for dictitem in v:
                        self.unique_colors(dictitem)
                elif isinstance(v, str):
                    if k == "color":
                        lstcolors.append(v)
                    else:
                        if k == "color":
                            lstcolors.append(v)
                            return list(set(lstcolors))
