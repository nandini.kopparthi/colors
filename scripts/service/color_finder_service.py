color_finder=blueprint('example_blueprint',__name__)
find_color_object=Find_color()
@color_finder.route('/',methods=['POST'])
def post_request():
    content=request.get_json()
    required_data=find_color_object.findcolor(content)
    return required_data